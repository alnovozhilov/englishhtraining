package com.example.englishtraining.task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.englishtraining.R;
import com.example.englishtraining.dictionary.MainDictionaryActivity;
import com.example.englishtraining.map.MainMapActivity;

public class MainTaskActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_task);
        Button vocabulary_button = findViewById(R.id.vocabulary_button);
        Button grammar_button = findViewById(R.id.grammar_button);
        vocabulary_button.setOnClickListener(this);
        grammar_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId())
        {
            case R.id.vocabulary_button:
                intent = new Intent(this, VocabularySettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.grammar_button:
                intent = new Intent(this, SectionsActivity.class);
                startActivity(intent);
                break;
        }
    }
}