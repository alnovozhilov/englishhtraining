package com.example.englishtraining.task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.englishtraining.R;
import com.example.englishtraining.dictionary.MainDictionaryActivity;

public class SectionsActivity extends AppCompatActivity {

    String[] SECTIONS = {"sect1", "sect2", "sect3", "sect4", "sect5", "sect6", "sect7", "sect8", "sect9", "sect10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sections);

        GridView gridView = (GridView) findViewById(R.id.sections_grid);

        SectionsActivity.SectionsAdapter sectionsAdapter = new SectionsActivity.SectionsAdapter();

        gridView.setAdapter(sectionsAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SectionsActivity.this, TaskCategoriesActivity.class);
                startActivity(intent);
            }
        });
    }

    class SectionsAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return SECTIONS.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.custom_grid_item, null);

            TextView textView =(TextView)view.findViewById(R.id.item_text);
            textView.setText(SECTIONS[position]);

            return view;
        }
    }
}