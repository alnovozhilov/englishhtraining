package com.example.englishtraining.task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.englishtraining.R;

public class VocabularySettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary_settings);

        final Spinner spinner = findViewById(R.id.categories);
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.cats, R.layout.custom_spinner_layout);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final Button accept_button = findViewById(R.id.accept_button);
        accept_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, VocabularyActivity.class);
        startActivity(intent);
    }
}