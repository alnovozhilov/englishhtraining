package com.example.englishtraining.map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.example.englishtraining.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;

public class MainMapActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnPoiClickListener {

    GoogleMap map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);

        SupportMapFragment mapFragment= (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        map = googleMap;
        LatLng School1 = new LatLng(56.0111586426642, 92.85670435613312);
        map.addMarker(new MarkerOptions().position(School1).title("Центр иностранных языков KLASS"));

        LatLng School2 = new LatLng(56.01245812311238, 92.86007527362386);
        map.addMarker(new MarkerOptions().position(School2).title("Абсолют English, Школа Иностранных Языков"));

        LatLng School3 = new LatLng(56.00860949572568, 92.8580502474382);
        map.addMarker(new MarkerOptions().position(School3).title("Real English For Travellers, Школа Иностранных Языков"));

        LatLng School4 = new LatLng(56.01074740013686, 92.86344258462131);
        map.addMarker(new MarkerOptions().position(School4).title("ЯЗЫКОМАНИЯ, центр изучения иностранных языков"));

        LatLng School5 = new LatLng(56.0098131226669, 92.84788346395221);
        map.addMarker(new MarkerOptions().position(School5).title("O`key, школа иностранных языков"));

        LatLng School6 = new LatLng(56.0140774012688, 92.86082056877535);
        map.addMarker(new MarkerOptions().position(School6).title("АBC, международная школа"));

        map.moveCamera(CameraUpdateFactory.newLatLng(School1));
        map.setOnPoiClickListener(this);

    }

    @Override
    public void onPoiClick(@NonNull PointOfInterest poi) {
        Toast.makeText(this, "Clicked: " +
                        poi.name + "\nPlace ID:" + poi.placeId +
                        "\nLatitude:" + poi.latLng.latitude +
                        " Longitude:" + poi.latLng.longitude,
                Toast.LENGTH_SHORT).show();
    }
}