package com.example.englishtraining.dictionary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.englishtraining.DatabaseHelper;
import com.example.englishtraining.MainActivity;
import com.example.englishtraining.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordsListActivity extends AppCompatActivity implements View.OnClickListener {

    //Переменная для работы с БД
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words_list);

        mDBHelper = new DatabaseHelper(this);

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        ListView listView = (ListView)findViewById(R.id.categories_list);
        final WordsListActivity.CategoriesAdapter categoriesAdapter = new WordsListActivity.CategoriesAdapter();

        listView.setAdapter(categoriesAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView word = (TextView)findViewById(R.id.word);
                Intent intent = new Intent(WordsListActivity.this, AddWordActivity.class);
                intent.putExtra("Word", word.getText());
                intent.putExtra("Category", getCategory());
                Bundle arguments = getIntent().getExtras();
                int Position = (int) arguments.get("Position");
                intent.putExtra("Position", Position);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TextView word = (TextView)findViewById(R.id.word);
                deleteRow(word.getText().toString());
                categoriesAdapter.reloadDatabase();
                categoriesAdapter.notifyDataSetChanged();
                return true;
            }
        });

        Button add_button = (Button)findViewById(R.id.add_button);
        add_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, AddWordActivity.class);
        intent.putExtra("Category", getCategory());

        Bundle arguments = getIntent().getExtras();
        int Position = (int) arguments.get("Position");
        intent.putExtra("Position", Position);
        startActivity(intent);
    }

    public void deleteRow(String word){
        mDb.delete("Dictionary", "Word = \"" + word + "\"", null);
    }

    public String getCategory() {
        Bundle arguments = getIntent().getExtras();
        int Position = (int) arguments.get("Position");
        List<String> categories = getCategories();
        return categories.get(Position);
    }

    public List<String> getWords(){
        List<String> words = new ArrayList<String>();

        Cursor cursor = mDb.rawQuery("SELECT Word FROM Dictionary WHERE Category=\"" + getCategory() + "\";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            words.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return words;
    }

    public List<String> getTranslations(){
        List<String> translations = new ArrayList<String>();

        Cursor cursor = mDb.rawQuery("SELECT Translation FROM Dictionary WHERE Category=\"" + getCategory() + "\";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            translations.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return translations;
    }

    public List<String> getCategories(){
        List<String> words = new ArrayList<String>();

        Cursor cursor = mDb.rawQuery("SELECT Name FROM Categories;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            words.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return words;
    }

    class CategoriesAdapter extends BaseAdapter {

        List<String> WORDS = getWords();
        List<String> TRANSLATIONS = getTranslations();

        @Override
        public int getCount() {
            return WORDS.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public void reloadDatabase(){
            WORDS = getWords();
            TRANSLATIONS = getTranslations();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.custom_words_item, null);
            TextView word =(TextView)view.findViewById(R.id.word);
            word.setText(WORDS.get(position));

            TextView translation =(TextView)view.findViewById(R.id.translation);
            translation.setText(TRANSLATIONS.get(position));

            return view;
        }
    }
}