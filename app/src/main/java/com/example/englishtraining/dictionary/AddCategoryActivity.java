package com.example.englishtraining.dictionary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.englishtraining.DatabaseHelper;
import com.example.englishtraining.R;

import java.io.IOException;

public class AddCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    //Переменная для работы с БД
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        mDBHelper = new DatabaseHelper(this);

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        Button add_button = (Button)findViewById(R.id.accept_button);
        add_button.setOnClickListener(this);

    }

    public void InsertToDatabase(String category){
        ContentValues values = new ContentValues();
        values.put("Name", category);
        mDb.insert("Categories", null, values);
    }

    @Override
    public void onClick(View v) {
        EditText category_input = (EditText)findViewById(R.id.category_input);
        InsertToDatabase(category_input.getText().toString());
        Intent intent = new Intent(this, MainDictionaryActivity.class);
        startActivity(intent);
    }
}