package com.example.englishtraining.dictionary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.englishtraining.DatabaseHelper;
import com.example.englishtraining.R;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import java.io.IOException;
import java.io.InputStream;


public class AddWordActivity extends AppCompatActivity implements View.OnClickListener{

    //Переменная для работы с БД
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDb;

    Translate translate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);

        mDBHelper = new DatabaseHelper(this);


        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        Button add_button = (Button)findViewById(R.id.accept_button);
        add_button.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        String mString = "default";

        Button translate_button = (Button)findViewById(R.id.translate_button);
        translate_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getTranslateService();
                translate();
            }
        });
    }

    public void InsertToDatabase(String word, String translation){

        ContentValues values = new ContentValues();

        Bundle arguments = getIntent().getExtras();

        String category = arguments.get("Category").toString();

        values.put("Category", category);
        values.put("Word", word);
        values.put("Translation", translation);

        if (arguments.get("Word") == null) {
            mDb.insert("Dictionary", null, values);
        }

        else{
            String update_word = arguments.get("Word").toString();
            mDb.update("Dictionary", values, "Word = \"" + update_word + "\"", null);
        }
    }

    @Override
    public void onClick(View v) {
        EditText word_input = (EditText)findViewById(R.id.word_input);
        EditText category_input = (EditText)findViewById(R.id.translate_input);
        InsertToDatabase(word_input.getText().toString(), category_input.getText().toString());
        Intent intent = new Intent(this, WordsListActivity.class);
        Bundle arguments = getIntent().getExtras();
        int Position = (int) arguments.get("Position");
        intent.putExtra("Position", Position);
        startActivity(intent);
    }

    public void getTranslateService() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try (InputStream is = getResources().openRawResource(R.raw.credentials)) {

            //Get credentials:
            final GoogleCredentials myCredentials = GoogleCredentials.fromStream(is);

            //Set credentials and get translate service:
            TranslateOptions translateOptions = TranslateOptions.newBuilder().setCredentials(myCredentials).build();
            translate = translateOptions.getService();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void translate() {

        EditText word_input = (EditText)findViewById(R.id.word_input);
        EditText translate_input = (EditText)findViewById(R.id.translate_input);

        //Get input text to be translated:
        String originalText = word_input.getText().toString();
        Translation translation = translate.translate(originalText, Translate.TranslateOption.targetLanguage("ru"), Translate.TranslateOption.model("base"));
        String translatedText = translation.getTranslatedText();

        //Translated text and original text are set to TextViews:
        translate_input.setText(translatedText);
    }
}